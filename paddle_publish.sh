# Copyright (c) 2022 Shenzhen Kaihong Digital Industry Development Co., Ltd. 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# To publish headers and libs
#!/bin/bash

PADDLE_SRC_PATH=$1
PADDLE_BINARY_PATH=$2
PADDLE_OUT_PATH=$3

mkdir -p "${PADDLE_OUT_PATH}/cxx/include"
mkdir "${PADDLE_OUT_PATH}/cxx/lib"
mkdir -p "${PADDLE_OUT_PATH}/lite/api"
cp "${PADDLE_SRC_PATH}/lite/api/paddle_api.h" "${PADDLE_OUT_PATH}/cxx/include"
cp "${PADDLE_SRC_PATH}/lite/api/paddle_lite_factory_helper.h" "${PADDLE_OUT_PATH}/cxx/include"
cp "${PADDLE_SRC_PATH}/lite/api/paddle_place.h" "${PADDLE_OUT_PATH}/cxx/include"
cp "${PADDLE_SRC_PATH}/lite/api/paddle_use_passes.h" "${PADDLE_OUT_PATH}/cxx/include"
cp "${PADDLE_SRC_PATH}/lite/api/paddle_lite_factory_helper.h" "${PADDLE_OUT_PATH}/cxx/include"
cp "${PADDLE_BINARY_PATH}/third_party/PaddleLite/paddle_use_kernels.h" "${PADDLE_OUT_PATH}/lite/api"
cp "${PADDLE_BINARY_PATH}/third_party/PaddleLite/paddle_use_ops.h" "${PADDLE_OUT_PATH}/lite/api"


