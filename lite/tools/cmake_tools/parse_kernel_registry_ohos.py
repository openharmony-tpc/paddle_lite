#!/usr/bin/env python
# Copyright (c) 2022 Shenzhen Kaihong Digital Industry Development Co., Ltd. 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function
import sys
import os
import logging
from ast import RegisterLiteKernelParser

if len(sys.argv) != 8:
    print("Error: parse_kernel_registry.py requires seven inputs!")
    exit(1)
kernels_list_path = sys.argv[1]
faked_kernels_list_path = sys.argv[2]
dest_path = sys.argv[3]
minkernels_list_path = sys.argv[4]
tailored = sys.argv[5]
with_extra = sys.argv[6]
enable_arm_fp16 = sys.argv[7]

print("dest_path=", dest_path)

if not os.path.exists(os.path.dirname(dest_path)):
    os.makedirs(os.path.dirname(dest_path))

if not os.path.exists(dest_path):
    print("dest_path is not exist,to create",dest_path)
    f=open(dest_path,mode='w+',encoding='utf-8')
    f.close()

if not os.path.exists(faked_kernels_list_path):
    print(faked_kernels_list_path+'文件不存在，开始创建')
    open(faked_kernels_list_path,mode='a',encoding='utf-8')


out_lines = [
    '#pragma once',
    '#include "paddle_lite_factory_helper.h"',
    '',
]
minlines = set()
if tailored == "ON":
    with open(minkernels_list_path) as fd:
        for line in fd:
            minlines.add(line.strip())
with open(kernels_list_path) as f:
    paths = set([path for path in f])
    print("***   paths=",paths)
    for path in paths:
        print("***   path=",path)
        with open(path.strip()) as g:
            c = g.read()
            kernel_parser = RegisterLiteKernelParser(c)
            kernel_parser.parse(with_extra, enable_arm_fp16)

            for k in kernel_parser.kernels:
                kernel = "%s,%s,%s,%s,%s" % (
                    k.op_type,
                    k.target,
                    k.precision,
                    k.data_layout,
                    k.alias, )
                if tailored == "ON":
                    if kernel not in minlines: continue
                key = "USE_LITE_KERNEL(%s, %s, %s, %s, %s);" % (
                    k.op_type,
                    k.target,
                    k.precision,
                    k.data_layout,
                    k.alias, )
                out_lines.append(key)

with open(faked_kernels_list_path) as f:
    paths = set([path for path in f])
    for path in paths:
        print("***in parse_kernel_registry path = ",path)
        with open(path.strip()) as g:
            c = g.read()
            kernel_parser = RegisterLiteKernelParser(c)
            kernel_parser.parse(with_extra, "ON")

            for k in kernel_parser.kernels:
                kernel = "%s,%s,%s,%s,%s" % (
                    k.op_type,
                    k.target,
                    k.precision,
                    k.data_layout,
                    k.alias, )
                if tailored == "ON":
                    if kernel not in minlines: continue
                key = "USE_LITE_KERNEL(%s, %s, %s, %s, %s);" % (
                    k.op_type,
                    k.target,
                    k.precision,
                    k.data_layout,
                    k.alias, )
                out_lines.append(key)

with open(dest_path, 'w') as f:
    logging.info("write kernel list to %s" % dest_path)
    f.write('\n'.join(out_lines))
